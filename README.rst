++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
README file for Scientific/Parallel-Computing, E.Perez
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This repository belongs to Elijah Perez.

Each folder corresponds to a homework assignment or lab assignment and contains its own README file.

Note that Homework 7 combines knowledge of parallel programming with knowledge on numerical solutions to partial differential equations (PDE's).
